﻿'Este proyecto fue creado por Mike Molina, estudiante de la Universidad de San Carlos de Guatemala.
'Repositorio Git https://bitbucket.org/LEO318x/lenguajes-formales-y-programaci-n-proyecto-1
'Visita mi página Web: http://leo318x.ml
Imports System.IO
Imports System.Reflection

Public Class Form1
    Dim contadorPestaña = 2
    Public sigueEstado As Integer = 0
    Public contador As Integer = 2
    Public contadorTokens = 1
    Public contadorErrores = 1
    '#     #                       #####                        
    '##   ## ###### #    # #    # #     # ##### #####  # #####  
    '# # # # #      ##   # #    # #         #   #    # # #    # 
    '#  #  # #####  # #  # #    #  #####    #   #    # # #    # 
    '#     # #      #  # # #    #       #   #   #####  # #####  
    '#     # #      #   ## #    # #     #   #   #   #  # #      
    '#     # ###### #    #  ####   #####    #   #    # # #

    Private Sub NuevaPestañaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NuevaPestañaToolStripMenuItem.Click
        nuevaPestaña()
    End Sub

    Private Sub CargarArchivoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CargarArchivoToolStripMenuItem.Click
        abrirArchivo()
    End Sub

    Private Sub GuardarArchivoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GuardarArchivoToolStripMenuItem.Click
        guardarArchivo()
    End Sub

    Private Sub GenerarGráficaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GenerarGráficaToolStripMenuItem.Click
        anailisisLexicoSintactico()
        generarArchivo()
        MsgBox("Se ha generado el archivo HTML")
    End Sub

    Private Sub VisualizarSalidaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VisualizarSalidaToolStripMenuItem.Click
        salidaArchivo()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        salir()
    End Sub

    Private Sub ManualDeAplicaciónToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ManualDeAplicaciónToolStripMenuItem.Click
        abrirManualdeUsuario()
    End Sub

    Private Sub AcercaDeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AcercaDeToolStripMenuItem.Click
        MsgBox("MIKE MOLINA - 2012-12535" & vbNewLine & "Lenguajes Formales y Programación B-")
    End Sub

    '                    ######                                                    
    '#####  ##### #    # #     # ######  ####  #####   ##   #    # #   ##    ####  
    '#    #   #   ##   # #     # #      #        #    #  #  ##   # #  #  #  #      
    '#####    #   # #  # ######  #####   ####    #   #    # # #  # # #    #  ####  
    '#    #   #   #  # # #       #           #   #   ###### #  # # # ######      # 
    '#    #   #   #   ## #       #      #    #   #   #    # #   ## # #    # #    # 
    '#####    #   #    # #       ######  ####    #   #    # #    # # #    #  #### 
    Private Sub btn_NuevaPestaña_Click(sender As Object, e As EventArgs) Handles btn_NuevaPestaña.Click
        nuevaPestaña()
    End Sub

    Private Sub btn_CerrarPestaña_Click(sender As Object, e As EventArgs) Handles btn_CerrarPestaña.Click
        cerrarPestaña()
    End Sub

    '#     #                      
    '##   ## ###### #    # #    # 
    '# # # # #      ##   # #    # 
    '#  #  # #####  # #  # #    # 
    '#     # #      #  # # #    # 
    '#     # #      #   ## #    # 
    '#     # ###### #    #  #### 

    Private Sub btnAbrirArchivo_Click(sender As Object, e As EventArgs) Handles btnAbrirArchivo.Click
        abrirArchivo()
    End Sub

    Private Sub btnGuardarArchivo_Click(sender As Object, e As EventArgs) Handles btnGuardarArchivo.Click
        guardarArchivo()
    End Sub

    Private Sub btnAnalizar_Click(sender As Object, e As EventArgs) Handles btnAnalizar.Click
        anailisisLexicoSintactico()
    End Sub

    Private Sub btnHTML_Click(sender As Object, e As EventArgs) Handles btnHTML.Click
        generarArchivo()
        salidaArchivo()
    End Sub

    '#######                                                    
    '#       #    # #    #  ####  #  ####  #    # ######  ####  
    '#       #    # ##   # #    # # #    # ##   # #      #      
    '#####   #    # # #  # #      # #    # # #  # #####   ####  
    '#       #    # #  # # #      # #    # #  # # #           # 
    '#       #    # #   ## #    # # #    # #   ## #      #    # 
    '#        ####  #    #  ####  #  ####  #    # ######  ####  

    Private Sub anailisisLexicoSintactico()
        TablaTokens.Items.Clear()
        TablaErrores.Items.Clear()
        Dim tmpTabPage3 As TabPage = TabControl_Pestañas.SelectedTab
        Dim txtBox As New Theme.AirFoxTextbox
        Try
            If (tmpTabPage3.Controls.ContainsKey("txtPestaña")) Then
                txtBox = CType(tmpTabPage3.Controls("txtPestaña"), Theme.AirFoxTextbox)
                Dim Analizar As New AnalizadorLexicoSintactico(txtBox.Text)
            End If
            contadorTokens = 1
            contadorErrores = 1
            MsgBox("¡Analisis terminado con exito!")
        Catch ex As Exception
            MsgBox("¡Ooops se ha encontrado un error! :(")
        End Try
    End Sub

    Private Sub salidaArchivo()
        Dim rutaDocumentos As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
        Dim rutaCompleta As String = rutaDocumentos + "\ResultadoHTML.html"
        'Corro el siguiente comando que inicia la abertura del archivo
        System.Diagnostics.Process.Start(rutaCompleta)
    End Sub

    Private Sub generarArchivo()
        Dim HTML As New GraficasHTML()
    End Sub

    Private Sub abrirArchivo()
        'Se crea el objeto OpenFileDialog que nos servira para abrir un archivo
        Dim openFileDialog1 As OpenFileDialog = New OpenFileDialog

        'Filtramos las extensiones de archivo que querramos abrir
        openFileDialog1.Filter = "Archivo EST (*.est)|*.est|Todos los Archvios (*.*)|*.*"
        openFileDialog1.FilterIndex = 1

        'Evitamos que seleccionen multiples archivos a la hora de abrir, se restringe a un solo archivo
        openFileDialog1.Multiselect = False

        'Variable que nos servira para tomar ciertas acciones del OpenFileDialog (Si el usuario cancela o afirma)
        Dim UserClickedOK As Boolean = openFileDialog1.ShowDialog

        'Si el usuario confirma abrir el archivo
        If (UserClickedOK) Then
            Try
                Dim fileStream As System.IO.Stream = openFileDialog1.OpenFile
                Dim nombreArchivo = System.IO.Path.GetFileNameWithoutExtension(openFileDialog1.FileName)
                Me.TabControl_Pestañas.SelectedTab.Text = nombreArchivo
                Try
                    Using reader As New System.IO.StreamReader(fileStream)
                        Dim tmpTabPage3 As TabPage = TabControl_Pestañas.SelectedTab
                        Dim txtBox As New Theme.AirFoxTextbox
                        If (tmpTabPage3.Controls.ContainsKey("txtPestaña")) Then
                            txtBox = CType(tmpTabPage3.Controls("txtPestaña"), Theme.AirFoxTextbox)
                            txtBox.Text = reader.ReadToEnd
                        End If
                    End Using
                    fileStream.Close()
                Catch ex As Exception
                    MsgBox("No hay ninguna pestaña abierta, el archivo no puede ser cargado")
                End Try

            Catch ex As Exception
                Console.WriteLine(ex)
            End Try
        End If
    End Sub

    Private Sub guardarArchivo()
        Dim SaveFileDialog1 As SaveFileDialog = New SaveFileDialog
        SaveFileDialog1.Filter = "Archivo EST (*.est)|*.est"
        Dim tmpTabPage3 As TabPage = TabControl_Pestañas.SelectedTab
        Dim txtBox As New Theme.AirFoxTextbox

        If SaveFileDialog1.ShowDialog = DialogResult.OK Then

            Try
                If (tmpTabPage3.Controls.ContainsKey("txtPestaña")) Then
                    txtBox = CType(tmpTabPage3.Controls("txtPestaña"), Theme.AirFoxTextbox)
                End If
                My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, txtBox.Text, False)
                tmpTabPage3.Text = System.IO.Path.GetFileNameWithoutExtension(SaveFileDialog1.FileName)
            Catch ex As Exception
                MsgBox("No hay ninguna pestaña abierta, el archivo no puede ser guardado")
            End Try

        End If
    End Sub

    Private Sub nuevaPestaña()
        'Se crea un objeto tipo TabPage
        Dim tmpPestaña As New TabPage

        'Se crea un objeto tipo TextBox y se le asignan ciertas propiedades
        Dim tmpTextBox As New Theme.AirFoxTextbox
        tmpTextBox.Name = "txtPestaña"
        tmpTextBox.MultiLine = True
        tmpTextBox.Location = New Point(2, 3)
        tmpTextBox.Width = 363
        tmpTextBox.Height = 425

        'Propiedades de la pestaña
        tmpPestaña.Text = "Pestaña " + contadorPestaña.ToString
        tmpPestaña.Name = "tabP_"

        'Agregamos la Pestaña al TabControl
        TabControl_Pestañas.TabPages.Add(tmpPestaña)

        'Agregamos el textbox a la Pestaña
        tmpPestaña.Controls.Add(tmpTextBox)

        'Sumamos +1 cada vez que se crea una pestaña
        contadorPestaña += 1
    End Sub

    Private Sub cerrarPestaña()
        'Se crea un Try para evitar que termine la ejecución del programa,
        'al no haber más pestañas que cerrar
        Try
            'Cierra la pestaña que esta seleccionada
            TabControl_Pestañas.TabPages.Remove(TabControl_Pestañas.SelectedTab)
        Catch ex As Exception
            MsgBox("Ooops ¿Hay pestañas?")
        End Try
    End Sub

    Private Sub abrirManualdeUsuario()
        'Obtengo la ruta en la cual se ejecuta la aplicación
        Dim rutaAplicacion As String = Path.GetDirectoryName(
            Assembly.GetExecutingAssembly().GetName().CodeBase)
        'Uno la ruta con el nombre del archivo para abrir
        Dim rutaCompleta As String = Path.Combine(rutaAplicacion, "Manual_de_Usuario_Practica_#2_LFP.pdf")
        'Corro el siguiente comando que inicia la abertura del pdf
        System.Diagnostics.Process.Start(rutaCompleta)
    End Sub

    Private Sub salir()
        Dim salirOpcion As Integer = MessageBox.Show("¿Estas seguro que deseas salir?", "Salir", MessageBoxButtons.YesNo)

        If (salirOpcion = DialogResult.Yes) Then
            'Fin del mundo OMG!!!!!!!!!!!!!!
            Application.Exit()
        End If
    End Sub

    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing


        If e.CloseReason = System.Windows.Forms.CloseReason.UserClosing Then
            'Cerrado desde la x del formulario o Alt + F4
            Dim salirOpcion As Integer = MessageBox.Show("¿Estas seguro que deseas salir?", "Salir", MessageBoxButtons.YesNo)

            If (salirOpcion = DialogResult.Yes) Then
                'Se provoca el fin del mundo!!!! OMG!!! XD
                e.Cancel = False
            Else
                'El mundo es salvado xD
                e.Cancel = True
            End If
        Else
            'Cerrado por otra razón      

        End If
    End Sub
End Class
