﻿Public Class AnalizadorLexicoSintactico
    Dim caracter() As Char
    Dim estado As Integer = 0
    Dim contadorFilas As Integer = 1
    Dim contadorColumnas As Integer = 1
    Dim tmpToken As String = ""
    Dim idToken As Integer = 0
    Dim cSintactico As Integer = 0
    Dim listaTokensValidos As New ArrayList
    Public Sub New(txtBox As String)
        Inicio(txtBox)
    End Sub

    Public Sub Inicio(texto As String)
        texto = texto.Replace(vbCrLf, vbLf)
        caracter = texto.ToCharArray
        lexicoSintactico()
        analisisSintactico()
    End Sub

    Public Sub lexicoSintactico()
        For pos = 0 To caracter.Count - 1
            Select Case estado
                Case 0
                    'Console.WriteLine("x0")
                    If (caracter(pos) = " " Or caracter(pos) = vbTab) Then
                        estado = 0
                        'contadorColumnas += 1
                        'Console.WriteLine("Espacio/Tabulación")
                    ElseIf (caracter(pos) = vbLf) Then
                        estado = 0
                        'Console.WriteLine("Salto de linea")
                        contadorFilas += 1
                        contadorColumnas = 0
                    ElseIf (caracter(pos) = "{" Or caracter(pos) = "}" Or caracter(pos) = "[" Or caracter(pos) = "]" Or caracter(pos) = ":" Or caracter(pos) = ";" Or caracter(pos) = "," Or caracter(pos) = "=" Or caracter(pos) = "-") Then
                        estado = 1
                        pos -= 1
                        contadorColumnas -= 1
                    ElseIf (caracter(pos) = """") Then
                        estado = 2
                        tmpToken += caracter(pos)
                        'contadorColumnas += 1
                        'pos -= 1
                    ElseIf (Char.IsLetter(caracter(pos))) Then
                        estado = 4
                        pos -= 1
                        contadorColumnas -= 1
                    ElseIf (Char.IsNumber(caracter(pos))) Then
                        estado = 5
                        pos -= 1
                        contadorColumnas -= 1
                    Else
                        xError(caracter(pos))
                    End If
                Case 1
                    tmpToken = caracter(pos)
                    If (revisarToken(tmpToken)) Then
                        tmpToken = ""
                        estado = 0
                    Else
                        tmpToken = ""
                        estado = 0
                    End If

                Case 2
                    If (caracter(pos) <> """") Then
                        If (pos = caracter.Count - 1) Then
                            tmpToken += caracter(pos)
                            xError(tmpToken)
                            estado = 0
                        Else
                            tmpToken += caracter(pos)
                            estado = 2
                        End If

                    ElseIf (caracter(pos) = """") Then
                        tmpToken += caracter(pos)
                        estado = 3
                        pos -= 1
                        contadorColumnas -= 1
                    End If
                Case 3
                    If (revisarToken(tmpToken)) Then
                        tmpToken = ""
                        estado = 0
                    Else
                        tmpToken = ""
                        estado = 0
                    End If
                Case 4
                    If (Char.IsLetter(caracter(pos))) Then
                        If (pos = caracter.Count - 1) Then
                            tmpToken += caracter(pos)
                            If (revisarToken(tmpToken)) Then
                                tmpToken = ""
                                estado = 0
                            Else
                                xError(tmpToken)
                                'estado = 0
                            End If
                        Else
                            tmpToken += caracter(pos)
                            estado = 4
                        End If

                    Else
                        If (revisarToken(tmpToken)) Then
                            tmpToken = ""
                        Else
                            xError(tmpToken)
                            'estado = 0
                        End If
                        tmpToken = ""
                        estado = 0
                        pos -= 1
                        contadorColumnas -= 1
                    End If
                Case 5
                    If (Char.IsNumber(caracter(pos))) Then
                        If (pos = caracter.Count - 1) Then
                            If (revisarToken(tmpToken)) Then
                                tmpToken += caracter(pos)
                                tmpToken = ""
                                estado = 0
                            End If
                        Else
                            tmpToken += caracter(pos)
                            estado = 5
                        End If
                    Else
                        If (revisarToken(tmpToken)) Then
                            tmpToken = ""
                            pos -= 1
                            contadorColumnas -= 1
                            estado = 0
                        End If
                    End If
                    'tmpToken = ""
                Case 99

            End Select
            contadorColumnas += 1
        Next
    End Sub

    Private Function revisarToken(tmpToken As String)
        Select Case tmpToken.ToLower
            Case "grafica"
                agregarToken(tmpToken, "Palabra Reservada Grafica", 1, contadorFilas, (contadorColumnas - tmpToken.Length).ToString)
                idToken = 1
                Return True
            Case "nombre"
                agregarToken(tmpToken, "Palabra Reservada Nombre", 2, contadorFilas, (contadorColumnas - tmpToken.Length).ToString)
                idToken = 2
                Return True
            Case "tipo"
                agregarToken(tmpToken, "Palabra Reservada Tipo", 3, contadorFilas, (contadorColumnas - tmpToken.Length).ToString)
                idToken = 3
                Return True
            Case "datos"
                agregarToken(tmpToken, "Palabra Reservada Datos", 4, contadorFilas, (contadorColumnas - tmpToken.Length).ToString)
                idToken = 4
                Return True
            Case "intervalo"
                agregarToken(tmpToken, "Palabra Reservada Intervalo", 5, contadorFilas, (contadorColumnas - tmpToken.Length).ToString)
                idToken = 5
                Return True
            Case "numero"
                agregarToken(tmpToken, "Numero Entero", 6, contadorFilas, (contadorColumnas - tmpToken.Length).ToString)
                idToken = 6
                Return True
            Case "{"
                agregarToken(tmpToken, "Apertura Llave", 7, contadorFilas, (contadorColumnas + 1 - tmpToken.Length).ToString)
                idToken = 7
                Return True
            Case "}"
                agregarToken(tmpToken, "Cierre Llave", 8, contadorFilas, (contadorColumnas + 1 - tmpToken.Length).ToString)
                idToken = 8
                Return True
            Case "["
                agregarToken(tmpToken, "Apertura Corchete", 9, contadorFilas, (contadorColumnas + 1 - tmpToken.Length).ToString)
                idToken = 9
                Return True
            Case "]"
                agregarToken(tmpToken, "Cierre Corchete", 10, contadorFilas, (contadorColumnas + 1 - tmpToken.Length).ToString)
                idToken = 10
                Return True
            Case ":"
                agregarToken(tmpToken, "Dos Puntos", 11, contadorFilas, (contadorColumnas + 1 - tmpToken.Length).ToString)
                idToken = 11
                Return True
            Case ";"
                agregarToken(tmpToken, "Punto y Coma", 12, contadorFilas, (contadorColumnas + 1 - tmpToken.Length).ToString)
                idToken = 12
                Return True
            Case ","
                agregarToken(tmpToken, "Coma", 13, contadorFilas, (contadorColumnas + 1 - tmpToken.Length).ToString)
                idToken = 13
                Return True
            Case "="
                agregarToken(tmpToken, "Igual", 14, contadorFilas, (contadorColumnas + 1 - tmpToken.Length).ToString)
                idToken = 14
                Return True
            Case "-"
                agregarToken(tmpToken, "Guion", 15, contadorFilas, (contadorColumnas + 1 - tmpToken.Length).ToString)
                idToken = 15
                Return True
            Case System.Text.RegularExpressions.Regex.Match(tmpToken.ToLower, "\""[^\""]*\""").Value
                agregarToken(tmpToken, "String", 16, contadorFilas, (contadorColumnas - tmpToken.Length).ToString)
                idToken = 16
                Return True
            Case System.Text.RegularExpressions.Regex.Match(tmpToken.ToLower, "\d+").Value
                agregarToken(tmpToken, "Numero Entero", 18, contadorFilas, (contadorColumnas - tmpToken.Length).ToString)
                idToken = 17
                Return True
            Case Else
                Return False
        End Select
    End Function

    Private Sub xError(token As String)
        agregarTokenError(token, contadorFilas, (contadorColumnas - tmpToken.Length).ToString)
        estado = 0
    End Sub

    Private Sub agregarToken(lexema As String, Token As String, tokenID As Integer, Fila As String, Columna As String)
        Dim tablaTokens As ListViewItem
        Form1.TablaTokens.BeginUpdate()
        tablaTokens = Form1.TablaTokens.Items.Add(Form1.contadorTokens)
        tablaTokens.SubItems.Add(lexema)
        tablaTokens.SubItems.Add(Token)
        tablaTokens.SubItems.Add(tokenID)
        tablaTokens.SubItems.Add("Fila " & Fila & ", Columna " & Columna)
        Form1.contadorTokens += 1
        Form1.TablaTokens.Update()
        Form1.TablaTokens.EndUpdate()
    End Sub

    Private Sub agregarTokenError(lexema As String, Fila As String, Columna As String)
        Dim tablaErrores As ListViewItem
        Form1.TablaErrores.BeginUpdate()
        tablaErrores = Form1.TablaErrores.Items.Add(Form1.contadorErrores)
        tablaErrores.SubItems.Add(lexema)
        tablaErrores.SubItems.Add("Fila " & Fila & ", Columna " & Columna)
        tablaErrores.SubItems.Add("Error Léxico, Token No Reconocido")
        Form1.contadorErrores += 1
        Form1.TablaErrores.Update()
        Form1.TablaErrores.EndUpdate()
    End Sub

    Private Sub agregarErrorSintactico(lexema As String, Posicion As String, tipoError As String)
        Dim tablaErrores As ListViewItem
        Form1.TablaErrores.BeginUpdate()
        tablaErrores = Form1.TablaErrores.Items.Add(Form1.contadorErrores)
        tablaErrores.SubItems.Add(lexema)
        tablaErrores.SubItems.Add(Posicion)
        tablaErrores.SubItems.Add(tipoError)
        Form1.contadorErrores += 1
        Form1.TablaErrores.Update()
        Form1.TablaErrores.EndUpdate()
    End Sub

    Public Sub analisisSintactico()
        Dim nombreB = False, tipoB = False, datosB = False, nombreBB = False, tipoBB = False, datosBB = False
        Dim cNombre = 1, cTipo = 1, cDatos = 1, cInicio = 0, cFinal = 0
        For iTokens = 0 To Form1.TablaTokens.Items.Count - 1
            If (cInicio = 0) Then
                If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "1") Then
                    agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba palabra reservada GRAFICA")
                End If
                cInicio += 1
            ElseIf (cInicio = 1) Then
                If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "7") Then
                    agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba {")
                End If
                nombreB = True
                tipoB = True
                datosB = True
                cInicio += 1
            Else
                If (nombreB And Form1.TablaTokens.Items(iTokens).SubItems(3).Text = "2" Or nombreBB) Then
                    nombreB = False
                    If (cNombre = 1) Then
                        cFinal += 1
                        nombreBB = True
                        cNombre += 1
                    Else
                        If (cNombre = 2) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "11") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba :")
                            End If
                            cNombre += 1
                        ElseIf (cNombre = 3) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "16") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba un STRING")
                            End If
                            cNombre += 1
                        ElseIf (cNombre = 4) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "12") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba ;")
                            End If
                            nombreBB = False
                        End If
                    End If
                ElseIf (tipoB And Form1.TablaTokens.Items(iTokens).SubItems(3).Text = "3" Or tipoBB) Then
                    tipoB = False
                    If (cTipo = 1) Then
                        cFinal += 1
                        tipoBB = True
                        cTipo += 1
                    Else
                        If (cTipo = 2) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "11") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba :")
                            End If
                            cTipo += 1
                        ElseIf (cTipo = 3) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "16") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba un STRING")
                            End If
                            cTipo += 1
                        ElseIf (cTipo = 4) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "12") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba ;")
                            End If
                            tipoBB = False
                        End If
                    End If
                ElseIf (datosB And Form1.TablaTokens.Items(iTokens).SubItems(3).Text = "4" Or datosBB) Then
                    datosB = False
                    If (cDatos = 1) Then
                        cFinal += 1
                        datosBB = True
                        cDatos += 1
                    Else
                        If (cDatos = 2) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "11") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba :")
                            End If
                            cDatos += 1
                        ElseIf (cDatos = 3) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "7") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba {")
                            End If
                            cDatos += 1
                        ElseIf (cDatos = 4) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "5") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba palabra reservada INTERVALO")
                            End If
                            cDatos += 1
                        ElseIf (cDatos = 5) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "18") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba un VALOR NUMERICO")
                            End If
                            cDatos += 1
                        ElseIf (cDatos = 6) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "14") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba =")
                            End If
                            cDatos += 1
                        ElseIf (cDatos = 7) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "7") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba {")
                            End If
                            cDatos += 1
                        ElseIf (cDatos = 8) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "9") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba [")
                            End If
                            cDatos += 1
                        ElseIf (cDatos = 9) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "18") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba un VALOR NUMERICO")
                            End If
                            cDatos += 1
                        ElseIf (cDatos = 10) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "15") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba -")
                            End If
                            cDatos += 1
                        ElseIf (cDatos = 11) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "18") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba un VALOR NUMERICO")
                            End If
                            cDatos += 1
                        ElseIf (cDatos = 12) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "10") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba ]")
                            End If
                            cDatos += 1
                        ElseIf (cDatos = 13) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "13") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba ,")
                            End If
                            cDatos += 1
                        ElseIf (cDatos = 14) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "18") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba un VALOR NUMERICO")
                            End If
                            cDatos += 1
                        ElseIf (cDatos = 15) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "8") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba }")
                            End If
                            cDatos += 1
                        ElseIf (cDatos = 16) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text <> "12") Then
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "ES - Se esperaba ;")
                            End If
                            cDatos += 1
                        ElseIf (cDatos = 17) Then
                            If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text = "5") Then
                                cDatos = 5
                            ElseIf (Form1.TablaTokens.Items(iTokens).SubItems(3).Text = "8") Then
                                datosBB = False
                            Else
                                agregarErrorSintactico(Form1.TablaTokens.Items(iTokens).SubItems(1).Text, Form1.TablaTokens.Items(iTokens).SubItems(4).Text, "Se esperaba palabra reservada INTERVALO o }")
                                datosBB = False
                            End If
                            'datosBB = False
                        End If
                    End If
                ElseIf (Form1.TablaTokens.Items(iTokens).SubItems(3).Text = "8" And cFinal = 3) Then
                    nombreB = False
                    tipoB = False
                    datosB = False
                    nombreBB = False
                    tipoBB = False
                    datosBB = False
                    cNombre = 1
                    cTipo = 1
                    cDatos = 1
                    cInicio = 0
                    cFinal = 0
                Else
                    'Console.WriteLine("---" & Form1.TablaTokens.Items(iTokens).SubItems(3).Text)
                    'Console.WriteLine("Se esperaba palabra reservada NOMBRE/TIPO/DATOS")
                End If

            End If

            '    If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text = "1" And iTokens = 0) Then
            'Else
            '    Console.WriteLine("Se esperaba grafica")
            'End If
            'If (Form1.TablaTokens.Items(iTokens).SubItems(3).Text = "7" And iTokens = 1) Then
            'Else
            '    Console.WriteLine("Se esperaba {")
            'End If
            'Console.WriteLine(Form1.TablaTokens.Items(iTokens).SubItems(3).Text)
        Next
    End Sub
End Class
