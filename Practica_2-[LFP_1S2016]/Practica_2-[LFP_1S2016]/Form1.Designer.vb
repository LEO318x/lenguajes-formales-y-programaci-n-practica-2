﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.MenuGeneral = New System.Windows.Forms.MenuStrip()
        Me.ArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevaPestañaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CargarArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GuardarArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GenerarGráficaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VisualizarSalidaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AyudaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ManualDeAplicaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AcercaDeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btn_NuevaPestaña = New System.Windows.Forms.Button()
        Me.btn_CerrarPestaña = New System.Windows.Forms.Button()
        Me.TablaTokens = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TablaErrores = New System.Windows.Forms.ListView()
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.BonfireGroupBox1 = New Practica_2__LFP_1S2016_.BonfireGroupBox()
        Me.AirFoxHeader21 = New Practica_2__LFP_1S2016_.Theme.AirFoxHeader2()
        Me.btnHTML = New Practica_2__LFP_1S2016_.BonfireButton()
        Me.btnAnalizar = New Practica_2__LFP_1S2016_.BonfireButton()
        Me.btnGuardarArchivo = New Practica_2__LFP_1S2016_.BonfireButton()
        Me.btnAbrirArchivo = New Practica_2__LFP_1S2016_.BonfireButton()
        Me.TabControl_Pestañas = New Practica_2__LFP_1S2016_.BonfireTabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.txtPestaña = New Practica_2__LFP_1S2016_.Theme.AirFoxTextbox()
        Me.MenuGeneral.SuspendLayout()
        Me.BonfireGroupBox1.SuspendLayout()
        Me.TabControl_Pestañas.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuGeneral
        '
        Me.MenuGeneral.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArchivoToolStripMenuItem, Me.AyudaToolStripMenuItem})
        Me.MenuGeneral.Location = New System.Drawing.Point(0, 0)
        Me.MenuGeneral.Name = "MenuGeneral"
        Me.MenuGeneral.Size = New System.Drawing.Size(740, 24)
        Me.MenuGeneral.TabIndex = 0
        Me.MenuGeneral.Text = "MenuStrip1"
        '
        'ArchivoToolStripMenuItem
        '
        Me.ArchivoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NuevaPestañaToolStripMenuItem, Me.CargarArchivoToolStripMenuItem, Me.GuardarArchivoToolStripMenuItem, Me.GenerarGráficaToolStripMenuItem, Me.VisualizarSalidaToolStripMenuItem, Me.SalirToolStripMenuItem})
        Me.ArchivoToolStripMenuItem.Name = "ArchivoToolStripMenuItem"
        Me.ArchivoToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.ArchivoToolStripMenuItem.Text = "&Archivo"
        '
        'NuevaPestañaToolStripMenuItem
        '
        Me.NuevaPestañaToolStripMenuItem.Name = "NuevaPestañaToolStripMenuItem"
        Me.NuevaPestañaToolStripMenuItem.Size = New System.Drawing.Size(160, 22)
        Me.NuevaPestañaToolStripMenuItem.Text = "&Nueva Pestaña"
        '
        'CargarArchivoToolStripMenuItem
        '
        Me.CargarArchivoToolStripMenuItem.Name = "CargarArchivoToolStripMenuItem"
        Me.CargarArchivoToolStripMenuItem.Size = New System.Drawing.Size(160, 22)
        Me.CargarArchivoToolStripMenuItem.Text = "&Cargar Archivo"
        '
        'GuardarArchivoToolStripMenuItem
        '
        Me.GuardarArchivoToolStripMenuItem.Name = "GuardarArchivoToolStripMenuItem"
        Me.GuardarArchivoToolStripMenuItem.Size = New System.Drawing.Size(160, 22)
        Me.GuardarArchivoToolStripMenuItem.Text = "&Guardar Archivo"
        '
        'GenerarGráficaToolStripMenuItem
        '
        Me.GenerarGráficaToolStripMenuItem.Name = "GenerarGráficaToolStripMenuItem"
        Me.GenerarGráficaToolStripMenuItem.Size = New System.Drawing.Size(160, 22)
        Me.GenerarGráficaToolStripMenuItem.Text = "&Generar Gráfica"
        '
        'VisualizarSalidaToolStripMenuItem
        '
        Me.VisualizarSalidaToolStripMenuItem.Name = "VisualizarSalidaToolStripMenuItem"
        Me.VisualizarSalidaToolStripMenuItem.Size = New System.Drawing.Size(160, 22)
        Me.VisualizarSalidaToolStripMenuItem.Text = "&Visualizar Salida"
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(160, 22)
        Me.SalirToolStripMenuItem.Text = "&Salir"
        '
        'AyudaToolStripMenuItem
        '
        Me.AyudaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ManualDeAplicaciónToolStripMenuItem, Me.AcercaDeToolStripMenuItem})
        Me.AyudaToolStripMenuItem.Name = "AyudaToolStripMenuItem"
        Me.AyudaToolStripMenuItem.Size = New System.Drawing.Size(53, 20)
        Me.AyudaToolStripMenuItem.Text = "&Ayuda"
        '
        'ManualDeAplicaciónToolStripMenuItem
        '
        Me.ManualDeAplicaciónToolStripMenuItem.Name = "ManualDeAplicaciónToolStripMenuItem"
        Me.ManualDeAplicaciónToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.ManualDeAplicaciónToolStripMenuItem.Text = "&Manual de Aplicación"
        '
        'AcercaDeToolStripMenuItem
        '
        Me.AcercaDeToolStripMenuItem.Name = "AcercaDeToolStripMenuItem"
        Me.AcercaDeToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.AcercaDeToolStripMenuItem.Text = "&Acerca de..."
        '
        'btn_NuevaPestaña
        '
        Me.btn_NuevaPestaña.Image = CType(resources.GetObject("btn_NuevaPestaña.Image"), System.Drawing.Image)
        Me.btn_NuevaPestaña.Location = New System.Drawing.Point(394, 61)
        Me.btn_NuevaPestaña.Name = "btn_NuevaPestaña"
        Me.btn_NuevaPestaña.Size = New System.Drawing.Size(41, 38)
        Me.btn_NuevaPestaña.TabIndex = 2
        Me.btn_NuevaPestaña.UseVisualStyleBackColor = True
        '
        'btn_CerrarPestaña
        '
        Me.btn_CerrarPestaña.Image = CType(resources.GetObject("btn_CerrarPestaña.Image"), System.Drawing.Image)
        Me.btn_CerrarPestaña.Location = New System.Drawing.Point(394, 105)
        Me.btn_CerrarPestaña.Name = "btn_CerrarPestaña"
        Me.btn_CerrarPestaña.Size = New System.Drawing.Size(41, 38)
        Me.btn_CerrarPestaña.TabIndex = 3
        Me.btn_CerrarPestaña.UseVisualStyleBackColor = True
        '
        'TablaTokens
        '
        Me.TablaTokens.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5})
        Me.TablaTokens.Location = New System.Drawing.Point(394, 248)
        Me.TablaTokens.Name = "TablaTokens"
        Me.TablaTokens.Size = New System.Drawing.Size(313, 97)
        Me.TablaTokens.TabIndex = 5
        Me.TablaTokens.UseCompatibleStateImageBehavior = False
        Me.TablaTokens.View = System.Windows.Forms.View.Details
        Me.TablaTokens.Visible = False
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "#"
        Me.ColumnHeader1.Width = 40
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Lexema"
        Me.ColumnHeader2.Width = 54
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Token"
        Me.ColumnHeader3.Width = 57
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "ID Token"
        Me.ColumnHeader4.Width = 88
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Posicion"
        '
        'TablaErrores
        '
        Me.TablaErrores.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader6, Me.ColumnHeader7, Me.ColumnHeader8, Me.ColumnHeader9})
        Me.TablaErrores.Location = New System.Drawing.Point(395, 367)
        Me.TablaErrores.Name = "TablaErrores"
        Me.TablaErrores.Size = New System.Drawing.Size(312, 97)
        Me.TablaErrores.TabIndex = 6
        Me.TablaErrores.UseCompatibleStateImageBehavior = False
        Me.TablaErrores.View = System.Windows.Forms.View.Details
        Me.TablaErrores.Visible = False
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "#"
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Lexema"
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Posición"
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Error"
        '
        'BonfireGroupBox1
        '
        Me.BonfireGroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.BonfireGroupBox1.Controls.Add(Me.AirFoxHeader21)
        Me.BonfireGroupBox1.Controls.Add(Me.btnHTML)
        Me.BonfireGroupBox1.Controls.Add(Me.btnAnalizar)
        Me.BonfireGroupBox1.Controls.Add(Me.btnGuardarArchivo)
        Me.BonfireGroupBox1.Controls.Add(Me.btnAbrirArchivo)
        Me.BonfireGroupBox1.Location = New System.Drawing.Point(457, 64)
        Me.BonfireGroupBox1.Name = "BonfireGroupBox1"
        Me.BonfireGroupBox1.Size = New System.Drawing.Size(271, 134)
        Me.BonfireGroupBox1.TabIndex = 4
        Me.BonfireGroupBox1.Text = "Funciones"
        '
        'AirFoxHeader21
        '
        Me.AirFoxHeader21.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.AirFoxHeader21.ForeColor = System.Drawing.Color.FromArgb(CType(CType(76, Byte), Integer), CType(CType(88, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.AirFoxHeader21.Location = New System.Drawing.Point(115, 3)
        Me.AirFoxHeader21.Name = "AirFoxHeader21"
        Me.AirFoxHeader21.Size = New System.Drawing.Size(42, 19)
        Me.AirFoxHeader21.TabIndex = 5
        Me.AirFoxHeader21.Text = "Menu"
        '
        'btnHTML
        '
        Me.btnHTML.ButtonStyle = Practica_2__LFP_1S2016_.BonfireButton.Style.Blue
        Me.btnHTML.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnHTML.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.btnHTML.Image = CType(resources.GetObject("btnHTML.Image"), System.Drawing.Image)
        Me.btnHTML.Location = New System.Drawing.Point(146, 83)
        Me.btnHTML.Name = "btnHTML"
        Me.btnHTML.RoundedCorners = True
        Me.btnHTML.Size = New System.Drawing.Size(84, 39)
        Me.btnHTML.TabIndex = 3
        Me.btnHTML.Text = "Salida"
        '
        'btnAnalizar
        '
        Me.btnAnalizar.ButtonStyle = Practica_2__LFP_1S2016_.BonfireButton.Style.Blue
        Me.btnAnalizar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnAnalizar.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.btnAnalizar.Image = CType(resources.GetObject("btnAnalizar.Image"), System.Drawing.Image)
        Me.btnAnalizar.Location = New System.Drawing.Point(42, 83)
        Me.btnAnalizar.Name = "btnAnalizar"
        Me.btnAnalizar.RoundedCorners = True
        Me.btnAnalizar.Size = New System.Drawing.Size(83, 39)
        Me.btnAnalizar.TabIndex = 2
        Me.btnAnalizar.Text = "Analizar"
        '
        'btnGuardarArchivo
        '
        Me.btnGuardarArchivo.ButtonStyle = Practica_2__LFP_1S2016_.BonfireButton.Style.Blue
        Me.btnGuardarArchivo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnGuardarArchivo.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.btnGuardarArchivo.Image = CType(resources.GetObject("btnGuardarArchivo.Image"), System.Drawing.Image)
        Me.btnGuardarArchivo.Location = New System.Drawing.Point(146, 37)
        Me.btnGuardarArchivo.Name = "btnGuardarArchivo"
        Me.btnGuardarArchivo.RoundedCorners = True
        Me.btnGuardarArchivo.Size = New System.Drawing.Size(84, 40)
        Me.btnGuardarArchivo.TabIndex = 1
        Me.btnGuardarArchivo.Text = "Guardar"
        '
        'btnAbrirArchivo
        '
        Me.btnAbrirArchivo.ButtonStyle = Practica_2__LFP_1S2016_.BonfireButton.Style.Blue
        Me.btnAbrirArchivo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnAbrirArchivo.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.btnAbrirArchivo.Image = CType(resources.GetObject("btnAbrirArchivo.Image"), System.Drawing.Image)
        Me.btnAbrirArchivo.Location = New System.Drawing.Point(42, 37)
        Me.btnAbrirArchivo.Name = "btnAbrirArchivo"
        Me.btnAbrirArchivo.RoundedCorners = True
        Me.btnAbrirArchivo.Size = New System.Drawing.Size(84, 40)
        Me.btnAbrirArchivo.TabIndex = 0
        Me.btnAbrirArchivo.Text = "Abrir"
        '
        'TabControl_Pestañas
        '
        Me.TabControl_Pestañas.Controls.Add(Me.TabPage1)
        Me.TabControl_Pestañas.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.TabControl_Pestañas.ItemSize = New System.Drawing.Size(0, 30)
        Me.TabControl_Pestañas.Location = New System.Drawing.Point(12, 27)
        Me.TabControl_Pestañas.Name = "TabControl_Pestañas"
        Me.TabControl_Pestañas.SelectedIndex = 0
        Me.TabControl_Pestañas.Size = New System.Drawing.Size(376, 466)
        Me.TabControl_Pestañas.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.txtPestaña)
        Me.TabPage1.Location = New System.Drawing.Point(4, 34)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(368, 428)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Pestaña 1"
        '
        'txtPestaña
        '
        Me.txtPestaña.Cursor = System.Windows.Forms.Cursors.Hand
        Me.txtPestaña.EnabledCalc = True
        Me.txtPestaña.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.txtPestaña.ForeColor = System.Drawing.Color.FromArgb(CType(CType(66, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(90, Byte), Integer))
        Me.txtPestaña.Location = New System.Drawing.Point(2, 3)
        Me.txtPestaña.MaxLength = 32767
        Me.txtPestaña.MultiLine = True
        Me.txtPestaña.Name = "txtPestaña"
        Me.txtPestaña.ReadOnly = False
        Me.txtPestaña.Size = New System.Drawing.Size(363, 425)
        Me.txtPestaña.TabIndex = 0
        Me.txtPestaña.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPestaña.UseSystemPasswordChar = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(740, 505)
        Me.Controls.Add(Me.TablaErrores)
        Me.Controls.Add(Me.TablaTokens)
        Me.Controls.Add(Me.BonfireGroupBox1)
        Me.Controls.Add(Me.btn_CerrarPestaña)
        Me.Controls.Add(Me.btn_NuevaPestaña)
        Me.Controls.Add(Me.TabControl_Pestañas)
        Me.Controls.Add(Me.MenuGeneral)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuGeneral
        Me.Name = "Form1"
        Me.Text = "Practica #2 - Mike Molina 2012-12535"
        Me.MenuGeneral.ResumeLayout(False)
        Me.MenuGeneral.PerformLayout()
        Me.BonfireGroupBox1.ResumeLayout(False)
        Me.TabControl_Pestañas.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuGeneral As MenuStrip
    Friend WithEvents ArchivoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents NuevaPestañaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CargarArchivoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GuardarArchivoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GenerarGráficaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents VisualizarSalidaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SalirToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AyudaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ManualDeAplicaciónToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AcercaDeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TabControl_Pestañas As BonfireTabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents txtPestaña As Theme.AirFoxTextbox
    Friend WithEvents btn_NuevaPestaña As Button
    Friend WithEvents btn_CerrarPestaña As Button
    Friend WithEvents BonfireGroupBox1 As BonfireGroupBox
    Friend WithEvents AirFoxHeader21 As Theme.AirFoxHeader2
    Friend WithEvents btnHTML As BonfireButton
    Friend WithEvents btnAnalizar As BonfireButton
    Friend WithEvents btnGuardarArchivo As BonfireButton
    Friend WithEvents btnAbrirArchivo As BonfireButton
    Friend WithEvents TablaTokens As ListView
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents ColumnHeader3 As ColumnHeader
    Friend WithEvents ColumnHeader4 As ColumnHeader
    Friend WithEvents ColumnHeader5 As ColumnHeader
    Friend WithEvents TablaErrores As ListView
    Friend WithEvents ColumnHeader6 As ColumnHeader
    Friend WithEvents ColumnHeader7 As ColumnHeader
    Friend WithEvents ColumnHeader8 As ColumnHeader
    Friend WithEvents ColumnHeader9 As ColumnHeader
End Class
